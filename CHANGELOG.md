# Changelog

All notable changes to this project will be documented in this file.

## [v1.2.6](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/3f17fad447b0eb838e7fe94ebb0757ab4c58a540)

### Update

- categories, taxs, description and short_description, optional for create product

## [v1.2.5](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/320d587eefd3192ad031e3a1b80077a3b8f045ef)

### New

- CWWYA_alertConnect and CWWYA_alertDisconnect, alert of connect or disconnect api to api

## [v1.2.4](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/42e2195b5516505e548f83a6424f83c1f95ab397)

### Fixed

- Fixed CWWYA_setApiByName and CWWYA_deleteApiByName

## [v1.2.3](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/1c70883cadcc25576a9a341121db3045a87b0780)

### New

- CWWYA_active and CWWYA_addApi functions for use by others plugins

## [v1.2.2](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/0badc4ce47d81b6f8416304db85316a453da37dc)

### New

- CWWYA_setApiByName and CWWYA_deleteApiByName functions for use by others plugins

## [v1.2.1](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/307cfabdf6295cae01e86564f13f4003e5d0bf2f)

### New

- Connect Woocommerce with your api requiere the plugin "Woocommerce"

## [v1.2.0](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/22851716147f11355ad218d4032fedb61867e55f)

### New

- Hook for detect product create, update and delete, to send info to apis

### Update

- Readme
- Description
- disabled mode debug

## [v1.1.5](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/15cab65f55a6aa8a9e36e5eab59e365054b34940)

### New

- Hook for detect user create, update and delete, to send info to apis

## [v1.1.4](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/8dca66284673af73cef0a4f3da4d423f0ac2fbca)

### New

- Hook for detect order create, update and delete, to send info to apis

## [v1.1.3](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/3cdea06d36667026cbd3bf9396b6f2b56dbf491f)

### New

- Validate Config Active for Routes

## [v1.1.2](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/473f2295abb2cb7d61427f7e23281cda36e664ec)

### Update

- Readme

## [v1.1.1](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/69fbef970215050cbff230562dbb66629338bc28)

### Update

- Style Page Config

## [v1.1.0](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/f895b83c7b3feef60167cfb8c18014213db6b483)

### New

- Rute Order for update

### Update

- [file] postman

### Fixed

- fixed create order

## [v1.0.18](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/a925091d95b87c50d0940ffe721247d41f04875d)

### New

- set payment method in update o create order

## [v1.0.17](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/0709bc734ee9f338bf6f78f70c104221c84bde0d)

### New

- set shipping method in update o create order

## [v1.0.16](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/7fb41d49edd133fb9095a470622b2ccf8d0879a7)

### New

- Rute Order for create

## [v1.0.15](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/095af290c695dfa321591d4804dc019f830bfa4d)

### New

- Rute User for update

### Fixed

- fixed create user role customer

## [v1.0.14](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/ec20bc9a47561ce54366c5e7a19c8ca1c567e8d8)

### New

- Rute User for create

## [v1.0.13](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/e6a7e514e25caeb05764cb64e4ebcb78491206bd)

### New

- Rute User for delete

### Fixed

- fixed error delete order
- fixed error delete product

## [v1.0.12](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/99d2be6e0190fe28bc4d0d24fd4fb8221fed47f9)

### New

- Rute Order for delete

## [v1.0.11](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/61431eaff0f810b809681852fbe0fff5b1828203)

### New

- Rute Product for update

### Update

- [file] postman

## [v1.0.10](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/26a149b031368ed5b3e4990912d506d40d240cf0)

### New

- Rute Product for delete

## [v1.0.9](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/7c698e5fcf57bb9f0f73d6a8d370279a551b017d)

### New

- Rute Product for create

## [v1.0.8](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/fcd2c380cae185576243c3691de828befdb304e2)

### New

- Rute User for view

## [v1.0.7](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/5782c143281aa764c38852955e74ada2e4ebc3ca)

### New

- Rute Order for view

### Fixed

- fixed CWWYA_getOrder

## [v1.0.6](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/bc37e8f3183065f9dbc21d5262737a83fc038be5)

### New

- Rute Product for view

## [v1.0.5](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/a961c9db3a4f668f69d0b8a1d5c639b0cafa1162)

### New

- hook-woo

## [v1.0.4](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/c607d76cfdf5906b9601cc83a2cf39182b0b6121)

### New

- Show Url Connect

## [v1.0.3](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/dda6ac62d0e973074b0e890035092642cccf9fc0)

### New

- Routes
- file postman

### Fixed

- fixed name enpoints
- fixed use base

## [v1.0.2](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/03053f6679d988d8a80b014c7b28814daa2d1e3f)

### New

- Page Config

## [v1.0.1](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/cad453ca23f3783c7e26830643a703dc9184815b)

### New

- Init Plugin
