# Connect Woocommerce with your api

This is a plugin that allows you to generate routes to connect your api with wordpress giving it the permissions you want, such as creating, editing and deleting products, orders and users. 
It will also send information on the creation, updating and deletion of orders, users and products to your apis.

- [Installing](#installing)
- [Configure](#configure)
- [Developer](#developer)
- [Repositories](#repositories)

# Installing

1) Download file in [here](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/master)

![alt download](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/raw/develop/tutorial/download.png)

2) Go to Dashboard in Wordpress 

3) Go to Plugins > Add New > Upload Plugin 

![alt install](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/raw/develop/tutorial/install.png)

4) Upload File > Active Plugin 

![alt active](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/raw/develop/tutorial/active.png)

# Configure

1) Go to Dashboard in Wordpress > Connect Woocommerce with your api

![alt openConfig](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/raw/develop/tutorial/openConfig.png)

2) Active Config

![alt activeConfig](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/raw/develop/tutorial/activeConfig.png)

3) Add New Api

![alt addNewApi](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/raw/develop/tutorial/addNewApi.png)

4) Add Name, Url, Token and select Permissions, and Save

![alt configApi](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/raw/develop/tutorial/configApi.png)

5) Use Url for Connect, use urls for connet your api with plugin in woocommerce

![alt useUrlForApi](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/raw/develop/tutorial/useUrlForApi.png)


## Developer
[Francisco Blanco](https://franciscoblanco.vercel.app/)

[Gitlab franciscoblancojn](https://gitlab.com/franciscoblancojn)

[Email blancofrancisco34@gmail.com](mailto:blancofrancisco34@gmail.com)

## Repositories

- [Gitlab](https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/master)