const onAddNewApi = () => {
  const contentApis = document.getElementById("contentApis");
  contentApis.innerHTML += `
        ${templateAddNewApi.split("newApiID").join(nItemsApis)}
    `;
  nItemsApis++;
};

const onLoadAddNewApi = () => {
  const addNewApi = document.getElementById("addNewApi");
  addNewApi.addEventListener("click", (e) => {
    e.preventDefault();
    onAddNewApi();
  });
};

const onDeleteApi = (submit) => {
    if(confirm("Are you sure?")){
        submit.click();
    }
};

const onLoadDeleteApi = () => {
  const btnsDeleteSubmit = document.querySelectorAll(".delete-api-submit");
  const btnsDelete = document.querySelectorAll(".delete-api");
  btnsDelete.forEach((btn, i) => {
    btn.addEventListener("click", (e) => {
      e.preventDefault();
      onDeleteApi(btnsDeleteSubmit[i]);
    });
  });
};

const onLoadPageConig = () => {
  onLoadAddNewApi();
  onLoadDeleteApi();
};

window.addEventListener("load", onLoadPageConig);
